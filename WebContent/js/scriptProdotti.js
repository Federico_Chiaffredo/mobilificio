/**
 * 
 */


function Checkbox(obj_cat) {

	let risultato = '<div class="form-check"><input class="form-check-input" type="checkbox" value="' + obj_cat.codice + '" id="' + obj_cat.codice + '"><label class="form-check-label" for="defaultCheck1">' + obj_cat.codice+' - '+obj_cat.nome + '</label></div>';

	return risultato;

}


function stampaCheckbox(arr_categorie) {
	let contenuto = "";

	for (let i = 0; i < arr_categorie.length; i++) {
		contenuto += Checkbox(arr_categorie[i]);
	}

	$("#checkbox").html(contenuto);
}





function aggiornaCheckbox() {
	$.ajax(
		{
			url: "http://localhost:8080/Mobilificio/recuperaCategorie",
			method: "POST",
			success: function(ris_success) {

				stampaCheckbox(ris_success);

			},
			error: function(ris_error) {
				console.log("ERRORE");
				console.log(ris_error);
			}
		}
	);
}




function aggiungiProdotto() {
	let codice = $("#codice").val();
	let nome = $("#nome").val();
	let prezzo = $("#prezzo").val();
	
      let categorie = [];
        $(':checkbox:checked').each(function(i){
          categorie[i] = $(this).val();
        });

	console.log(categorie);
	$.ajax(
		{
			url: "http://localhost:8080/Mobilificio/inserisciProdotto",
			method: "POST",
			data: {
				cod: codice,
				nom: nome,
				prz: prezzo,
				cat: JSON.stringify(categorie)
			},
			success: function(risultato) {
				alert("Prodotto aggiunto con successo");
				/*aggiornaTabella();*/
				$("#insertModal").modal("toggle");

			},
			error: function(ris_error) {
				console.log("ERRORE");
				console.log(ris_error);
			}
		}
	);
}








function aggiornaTabella() {
	$.ajax(
		{
			url: "http://localhost:8080/Mobilificio/recuperaProdotti",
			method: "POST",
			success: function(ris_success) {

				stampaTabella(ris_success);

			},
			error: function(ris_error) {
				console.log("ERRORE");
				console.log(ris_error);
			}
		}
	);
}

function stampaTabella(arr_categorie) {
	let contenuto = "";

	for (let i = 0; i < arr_categorie.length; i++) {
		contenuto += Tabella(arr_categorie[i]);
	}

	$("#contenuto-richiesta").html(contenuto);
}



function Tabella(obj_cat) {

	let risultato = '<tr data-identificativo="' + obj_cat.codice + '">';
	risultato += '    <td>' + obj_cat.codice + '</td>';
	risultato += '    <td>' + obj_cat.nome + '</td>';
	risultato += '    <td>' + obj_cat.prezzo + '</td>';
	risultato += '    <td><button type="button" id="cta-canc" class="btn btn-danger btn-block" onclick="eliminaProdotto(this)">Elimina</button></td>';
	risultato += '    <td><button type="button" class="btn btn-warning btn-block" onclick="prendiProdotto(this)">Modifica</button></td>';
	risultato += '</tr>';

	return risultato;

}




function eliminaProdotto(objButton) {
	let codice = $(objButton).parent().parent().data("identificativo");

	$.ajax(
		{
			url: "http://localhost:8080/Mobilificio/cancellaProdotto",
			method: "POST",
			data: {
				cod: codice
			},
			success: function(responso) {
				alert("Categoria eliminata con successo");
				aggiornaTabella();
			},
			error: function(errore) {
				console.log(errore);
			}
		}
	);



}




function prendiProdotto(objButton) {
	let codice = $(objButton).parent().parent().data("identificativo");
	
	$.ajax(
			{
				url: "http://localhost:8080/Mobilificio/recuperaProdotto",
				method: "POST",
				data: {
					cod:codice
				},
				success: function(responso){
					
					
					$("#codice_edit").val(responso.codice);
					$("#nome_edit").val(responso.nome);
					$("#prezzo_edit").val(responso.prezzo);

					$("#ModaleModifica").modal("show");
					
					
				},
				error: function(errore){
					console.log(errore);
				}
			}
	);
}



$(document).ready(
	function() {
		aggiornaTabella();
		aggiornaCheckbox();
		
		$("#cta-inserisci").click(
			function(){
				aggiungiProdotto();
				aggiornaTabella();
			}
		);
		
		$("#cta-ricarica").click(
			function(){
				aggiornaTabella();
			}
		);
		
		
			$("#cta-modifica").click(
			function() {
				let codice = $("#codice_edit").val();
				let nome = $("#nome_edit").val();
				let prezzo = $("#prezzo_edit").val();

				$.ajax(
					{
						url: "http://localhost:8080/Mobilificio/aggiornaProdotti",
						method: "POST",
						data: {
							cod: codice,
							nom: nome,
							prz: prezzo
						},
						success: function(responso) {
							alert("Modifica effettuata");
							aggiornaTabella();
							$("#ModaleModifica").modal("toggle");


						},
						error: function(errore) {
							console.log(errore);
						}
					}
				);
			}
		);
		
		
	}
);







