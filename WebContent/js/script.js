/**
 * 
 */
function stampaTabella(arr_categorie) {
	let contenuto = "";

	for (let i = 0; i < arr_categorie.length; i++) {
		contenuto += Tabella(arr_categorie[i]);
	}

	$("#contenuto-richiesta").html(contenuto);
}



function eliminaCategoria(objButton) {
	let codice = $(objButton).parent().parent().data("identificativo");

	$.ajax(
		{
			url: "http://localhost:8080/Mobilificio/cancellaCategorie",
			method: "POST",
			data: {
				cod: codice
			},
			success: function(responso) {
				alert("Categoria eliminata con successo");
				aggiornaTabella();
			},
			error: function(errore) {
				console.log(errore);
			}
		}
	);



}


function Tabella(obj_cat) {

	let risultato = '<tr data-identificativo="' + obj_cat.codice + '">';
	risultato += '    <td>' + obj_cat.codice + '</td>';
	risultato += '    <td>' + obj_cat.nome + '</td>';
	risultato += '    <td>' + obj_cat.descrizione + '</td>';
	risultato += '    <td><button type="button" id="cta-canc" class="btn btn-danger btn-block" onclick="eliminaCategoria(this)">Elimina</button></td>';
	risultato += '    <td><button type="button" class="btn btn-warning btn-block" onclick="prendiCategoria(this)">Modifica</button></td>';
	risultato += '</tr>';

	return risultato;

}




function prendiCategoria(objButton) {
	let codice = $(objButton).parent().parent().data("identificativo");
	
	$.ajax(
			{
				url: "http://localhost:8080/Mobilificio/recuperaCategoria",
				method: "POST",
				data: {
					cod:codice
				},
				success: function(responso){
					
					
					$("#codice_edit").val(responso.codice);
					$("#nome_edit").val(responso.nome);
					$("#descrizione_edit").val(responso.descrizione);

					$("#ModaleModifica").modal("show");
					
					
				},
				error: function(errore){
					console.log(errore);
				}
			}
	);
}







function aggiornaTabella() {
	$.ajax(
		{
			url: "http://localhost:8080/Mobilificio/recuperaCategorie",
			method: "POST",
			success: function(ris_success) {

				stampaTabella(ris_success);

			},
			error: function(ris_error) {
				console.log("ERRORE");
				console.log(ris_error);
			}
		}
	);
}


function mettiInTabella() {
	let codice = $("#codice").val();
	let nome = $("#nome").val();
	let descrizione = $("#descrizione").val();
	$.ajax(
		{
			url: "http://localhost:8080/Mobilificio/inserisciCategorie",
			method: "POST",
			data: {
				cod: codice,
				nom: nome,
				descr: descrizione
			},
			success: function(risultato) {
				alert("Categoria aggiunta con successo");
				aggiornaTabella();
				$("#insertModal").modal("toggle");

			},
			error: function(ris_error) {
				console.log("ERRORE");
				console.log(ris_error);
			}
		}
	);
}





$(document).ready(
	function() {
		aggiornaTabella();

		$("#cta-ricarica").click(
			function() {
				aggiornaTabella();
			}
		);
		
		

		$("#cta-inserisci").click(
			function() {
				mettiInTabella();
			}
		);

		$("#cta-modifica").click(
			function() {
				let codice = $("#codice_edit").val();
				let nome = $("#nome_edit").val();
				let descrizione = $("#descrizione_edit").val();

				$.ajax(
					{
						url: "http://localhost:8080/Mobilificio/aggiornaCategorie",
						method: "POST",
						data: {
							cod: codice,
							nom: nome,
							descr: descrizione
						},
						success: function(responso) {
							alert("Modifica effettuata");
							aggiornaTabella();
							$("#ModaleModifica").modal("toggle");


						},
						error: function(errore) {
							console.log(errore);
						}
					}
				);
			}
		);



	}
);