DROP DATABASE IF EXISTS mobilificio;
CREATE DATABASE mobilificio;
USE mobilificio;

CREATE TABLE categorie(
	categoriaid INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    codice VARCHAR(20) NOT NULL UNIQUE,
    nome VARCHAR(250) NOT NULL,
	descrizione VARCHAR(250)
);


	CREATE TABLE prodotti(
    prodottoid INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nome VARCHAR(150) NOT NULL,
    codice VARCHAR(150) NOT NULL UNIQUE,
    prezzo VARCHAR(20) NOT NULL
    );
    
    
    
    CREATE TABLE categorie_prodotti(
    refcat INTEGER NOT NULL,
    refprod INTEGER NOT NULL,
    PRIMARY KEY(refcat,refprod),
    FOREIGN KEY (refcat) REFERENCES categorie(categoriaid) ON DELETE CASCADE,
    FOREIGN KEY (refcat) REFERENCES prodotti(prodottoid) ON DELETE CASCADE
    );

INSERT INTO categorie (codice,nome,descrizione) VALUES
("ARR10","Arredo","Elementi di arredo"),
("ILL10","Illuminazione","Elementi di illuminazione"),
("TEC10","Tecnologia","Strumenti tecnologici"),
("UFF20","Ufficio","Roba da ufficio"),
("CANC23","Cancelleria","Cose di cancelleria");


INSERT INTO prodotti (nome,codice,prezzo) VALUES
("Sedia","CC","12.00"),
("Computer","AA","12.00"),
("Mouse","BS","12.00"),
("Amaca","HJK","12.00"),
("Penna","UI","12.00"),
("Lampadina","JKL","12.00");


INSERT INTO categorie_prodotti (refcat,refprod) VALUES
(1,1),
(1,4),
(2,2),
(2,3);




SELECT * FROM categorie;
SELECT * FROM prodotti;
SELECT * FROM categorie_prodotti;

SELECT categorie.codice FROM prodotti 
JOIN categorie_prodotti ON prodotti.prodottoid=categorie_prodotti.refprod
JOIN categorie ON categorie.categoriaid=categorie_prodotti.refcat WHERE prodotti.codice="CC";