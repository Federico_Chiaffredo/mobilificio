package com.mobilificio.api.services;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import com.mobilificio.api.connessione.ConnettoreDB;
import com.mobilificio.api.model.Categoria;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

public class CategoriaDAO implements DAO<Categoria>{

	@Override
	public Categoria getByCod(String codice) throws SQLException {
		
		Categoria cat=new Categoria();
		Connection conn=(Connection) ConnettoreDB.getIstanza().getConnessione();
		String query="SELECT categoriaid,codice,nome,descrizione FROM categorie WHERE codice=?";
		PreparedStatement ps=(PreparedStatement) conn.prepareStatement(query);
		ps.setString(1, codice);
		
		ResultSet ris=ps.executeQuery();
		ris.next();
		cat.setId(ris.getInt(1));
		cat.setCodice(ris.getString(2));
		cat.setNome(ris.getString(3));
		cat.setDescrizione(ris.getString(4));
		
		return cat;
	}

	@Override
	public ArrayList<Categoria> getAll() throws SQLException {
		ArrayList<Categoria> elenco = new ArrayList<Categoria>();
	       
   		Connection conn = (Connection) ConnettoreDB.getIstanza().getConnessione();
           
       	String query = "SELECT categoriaid,codice,nome,descrizione FROM categorie";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	
       	ResultSet risultato = ps.executeQuery();
       	while(risultato.next()){
       		Categoria temp = new Categoria();
       		temp.setId(risultato.getInt(1));
       		temp.setCodice(risultato.getString(2));
       		temp.setNome(risultato.getString(3));
       		temp.setDescrizione(risultato.getString(4));
       		elenco.add(temp);
       	}
       	
       	return elenco;
	}

	@Override
	public void insert(Categoria t) throws SQLException {
		Connection conn = (Connection) ConnettoreDB.getIstanza().getConnessione();

   		String query = "INSERT INTO categorie (codice,nome,descrizione) VALUES (?,?,?)";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
       	ps.setString(1, t.getCodice());
       	ps.setString(2, t.getNome());
       	ps.setString(3, t.getDescrizione());
       	
       	ps.executeUpdate();
       	ResultSet risultato = ps.getGeneratedKeys();
       	risultato.next();
   		
       	t.setId(risultato.getInt(1));
		
	}

	@Override
	public boolean delete(Categoria t) throws SQLException {
		Connection conn = (Connection) ConnettoreDB.getIstanza().getConnessione();
   		String query = "DELETE FROM categorie WHERE codice = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setString(1, t.getCodice());
       	
       	int affRows = ps.executeUpdate();
       	if(affRows > 0)
       		return true;
       	
   		return false;
	}

	@Override
	public boolean update(Categoria t) throws SQLException {
		Connection conn = (Connection) ConnettoreDB.getIstanza().getConnessione();
   		String query = "UPDATE categorie SET "
   				+ "codice = ?, "
   				+ "nome = ?, "
   				+ "descrizione=? "
   				+ "WHERE codice= ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setString(1, t.getCodice());
       	ps.setString(2, t.getNome());
       	ps.setString(3, t.getDescrizione());
       	ps.setString(4, t.getCodice());
       	
       	int affRows = ps.executeUpdate();
       	if(affRows > 0)
       		return true;
       	
   		return false;
	}

	
	
}
