package com.mobilificio.api.services;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.mobilificio.api.connessione.ConnettoreDB;
import com.mobilificio.api.model.Categoria;
import com.mobilificio.api.model.Prodotto;
import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

public class ProdottoDAO implements DAO<Prodotto>{

	@Override
	public Prodotto getByCod(String codice) throws SQLException {
		Prodotto prod=new Prodotto();
		Connection conn=(Connection) ConnettoreDB.getIstanza().getConnessione();
		String query="SELECT prodottoid,nome,codice,prezzo FROM prodotti WHERE codice=?";
		PreparedStatement ps=(PreparedStatement) conn.prepareStatement(query);
		ps.setString(1, codice);
		
		ResultSet ris=ps.executeQuery();
		ris.next();
		prod.setId(ris.getInt(1));
		prod.setNome(ris.getString(2));
		prod.setCodice(ris.getString(3));
		prod.setPrezzo(ris.getString(4));
		
		
		
		
		return prod;
	}

	@Override
	public ArrayList getAll() throws SQLException {
		ArrayList<Prodotto> elenco = new ArrayList<Prodotto>();
	       
   		Connection conn = (Connection) ConnettoreDB.getIstanza().getConnessione();
           
       	String query = "SELECT prodottoid,nome,codice,prezzo FROM prodotti";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	
       	ResultSet risultato = ps.executeQuery();
       	while(risultato.next()){
       		Prodotto temp = new Prodotto(); 
       		temp.setId(risultato.getInt(1));
       		temp.setNome(risultato.getString(2));
       		temp.setCodice(risultato.getString(3));
       		temp.setPrezzo(risultato.getString(4));
       		elenco.add(temp);
       	}
       	
       	return elenco;
	}

	@Override
	public void insert(Prodotto t) throws SQLException {
		Connection conn = (Connection) ConnettoreDB.getIstanza().getConnessione();

   		String query = "INSERT INTO prodotti (nome,codice,prezzo) VALUES (?,?,?)";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
       	ps.setString(1, t.getNome());
       	ps.setString(2, t.getCodice());
       	ps.setString(3, t.getPrezzo());
       	
       	ps.executeUpdate();
       	ResultSet risultato = ps.getGeneratedKeys();
       	risultato.next();
   		
       	t.setId(risultato.getInt(1));
		
       	linkaCategorie(t);
	}
	
	
	public void linkaCategorie(Prodotto t) throws SQLException {
		Connection conn = (Connection) ConnettoreDB.getIstanza().getConnessione();

   		String query = "INSERT INTO categorie_prodotti (refcat,refprod) VALUES (?,?)";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
       	int prodID=t.getId();
       	
       	for(int i=0;i<t.getCat().size();i++) {
       		ps.setInt(1, t.getCat().get(i).getId());
       		ps.setInt(2, t.getId());
       		ps.executeUpdate();
       	}
	}
	
	
	
	
	
	
	public ArrayList<String> recuperaCategorie(Prodotto t) throws SQLException {
		ArrayList<String> elenco=new ArrayList<String>();
		String a="";
		Connection conn = (Connection) ConnettoreDB.getIstanza().getConnessione();

   		String query = "SELECT categorie.codice FROM prodotti "
   				+ "JOIN categorie_prodotti ON prodotti.prodottoid=categorie_prodotti.refprod "
   				+ "JOIN categorie ON categorie.categoriaid=categorie_prodotti.refcat WHERE prodotti.codice=? ";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setString(1, t.getCodice());
       	
       	ResultSet ris= ps.executeQuery();
       	while(ris.next()) {
       		elenco.add(ris.getString(1));
       	}
       	
       	return elenco;
	
	}

	@Override
	public boolean delete(Prodotto t) throws SQLException {
		Connection conn = (Connection) ConnettoreDB.getIstanza().getConnessione();
   		String query = "DELETE FROM prodotti WHERE codice = ?";
       	PreparedStatement ps = (PreparedStatement) conn.prepareStatement(query);
       	ps.setString(1, t.getCodice());
       	
       	int affRows = ps.executeUpdate();
       	if(affRows > 0)
       		return true;
       	
   		return false;
	}

	@Override
	public boolean update(Prodotto t) throws SQLException {
		// TODO Auto-generated method stub
		return false;
	}

	



	

}
