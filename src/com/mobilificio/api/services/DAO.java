package com.mobilificio.api.services;

import java.sql.SQLException;
import java.util.ArrayList;

public interface DAO <T>{
	
	T getByCod(String codice) throws SQLException;
	
	ArrayList<T> getAll() throws SQLException;
	
	void insert(T t) throws SQLException;
	
	boolean delete(T t) throws SQLException;
	
	boolean update(T t) throws SQLException;
	
}
