package com.mobilificio.api.model;

public class Categoria {
	private int id;
	private String codice;
	private String nome;
	private String descrizione;
	
	public Categoria() {
		
	}
	public Categoria(String codice,String nome,String descrizione){
		this.codice=codice;
		this.nome=nome;
		this.descrizione=descrizione;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCodice() {
		return codice;
	}
	public void setCodice(String codiceString) {
		this.codice = codiceString;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getDescrizione() {
		return descrizione;
	}
	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}
	@Override
	public String toString() {
		return "Categoria [id=" + id + ", codiceString=" + codice + ", nome=" + nome + ", descrizione="
				+ descrizione + "]";
	}
	
	
	
	
	
	
}
