package com.mobilificio.api.model;

import java.util.ArrayList;

public class Prodotto {
	private int id;
	private String nome;
	private String codice;
	private String prezzo;
	private ArrayList<Categoria> cat=new ArrayList<Categoria>();
	
	
	public Prodotto(){
		
	}
	public Prodotto(String nome,String codice,String prezzo,ArrayList<Categoria> categoria){
		this.nome=nome;
		this.codice=codice;
		this.prezzo=prezzo;
		this.cat=categoria;
	}
	
	public ArrayList<Categoria> getCat() {
		return cat;
	}
	public void setCat(ArrayList<Categoria> cat) {
		this.cat = cat;
	}
	
	
	public ArrayList<Categoria> addCat(Categoria c){
		this.cat.add(c);
		return cat;
	}
	
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCodice() {
		return codice;
	}
	public void setCodice(String codice) {
		this.codice = codice;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getPrezzo() {
		return prezzo;
	}
	public void setPrezzo(String prezzo) {
		this.prezzo = prezzo;
	}
	@Override
	public String toString() {
		return "Prodotto [id=" + id + ", nome=" + nome + ", prezzo=" + prezzo + "]";
	}
	
	
}
