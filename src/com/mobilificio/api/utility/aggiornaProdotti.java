package com.mobilificio.api.utility;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mobilificio.api.model.Categoria;
import com.mobilificio.api.model.Prodotto;
import com.mobilificio.api.services.CategoriaDAO;
import com.mobilificio.api.services.ProdottoDAO;

/**
 * Servlet implementation class aggiornaProdotti
 */
@WebServlet("/aggiornaProdotti")
public class aggiornaProdotti extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String codice=request.getParameter("cod");
		String nome=request.getParameter("nom");
		String descrizione=request.getParameter("descr");
		PrintWriter out = response.getWriter();
		ProdottoDAO cdao=new ProdottoDAO();
		
		try {
			Prodotto temp=cdao.getByCod(codice);
			temp.setCodice(codice);
			temp.setNome(nome);
			temp.setPrezzo(descrizione);
			cdao.update(temp);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
