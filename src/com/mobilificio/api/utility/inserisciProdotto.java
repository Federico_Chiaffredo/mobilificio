package com.mobilificio.api.utility;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mobilificio.api.model.Categoria;
import com.mobilificio.api.model.Prodotto;
import com.mobilificio.api.services.CategoriaDAO;
import com.mobilificio.api.services.ProdottoDAO;

/**
 * Servlet implementation class inserisciProdotto
 */
@WebServlet("/inserisciProdotto")
public class inserisciProdotto extends HttpServlet {

//	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//		
//	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String nome=request.getParameter("nom");
		String codice=request.getParameter("cod");
		String prezzo=request.getParameter("prz");
		String cate=request.getParameter("cat");
		String[]categ=cate.replace("[", "").replace("]", "").replace("\"", "").split(",");
		
		ArrayList<Categoria> elCategorie=new ArrayList<Categoria>();
		CategoriaDAO cdao=new CategoriaDAO();
		
		for(int i=0;i<categ.length;i++) {
			try {
				Categoria temp=cdao.getByCod(categ[i]);
				elCategorie.add(temp);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		
		ProdottoDAO pdao=new ProdottoDAO();
		Prodotto ptemp=new Prodotto(nome,codice,prezzo,elCategorie);
		try {
			pdao.insert(ptemp);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
	}

}
