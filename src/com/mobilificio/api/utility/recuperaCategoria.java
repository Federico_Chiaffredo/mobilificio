package com.mobilificio.api.utility;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.mobilificio.api.model.Categoria;
import com.mobilificio.api.services.CategoriaDAO;

/**
 * Servlet implementation class recuperaCategoria
 */
@WebServlet("/recuperaCategoria")
public class recuperaCategoria extends HttpServlet {

//	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//	
//	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		CategoriaDAO cdao=new CategoriaDAO();
		Gson jasonizzatore=new Gson();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		String codice=request.getParameter("cod");
		
		
		try {
			Categoria cat=cdao.getByCod(codice);
			String catJson=jasonizzatore.toJson(cat);
			out.print(catJson);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
