package com.mobilificio.api.utility;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mobilificio.api.model.Categoria;
import com.mobilificio.api.services.CategoriaDAO;

/**
 * Servlet implementation class cancellaCategorie
 */
@WebServlet("/cancellaCategorie")
public class cancellaCategorie extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String codice=request.getParameter("cod");
		CategoriaDAO cdao=new CategoriaDAO();
		
		try {
			Categoria temp=cdao.getByCod(codice);
			cdao.delete(temp);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

}
