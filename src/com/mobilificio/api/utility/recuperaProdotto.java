package com.mobilificio.api.utility;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.mobilificio.api.model.Categoria;
import com.mobilificio.api.model.Prodotto;
import com.mobilificio.api.services.CategoriaDAO;
import com.mobilificio.api.services.ProdottoDAO;

/**
 * Servlet implementation class recuperaProdotto
 */
@WebServlet("/recuperaProdotto")
public class recuperaProdotto extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PrintWriter out = response.getWriter();
		ProdottoDAO cdao=new ProdottoDAO();
		Gson jasonizzatore=new Gson();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		String codice=request.getParameter("cod");
		
		
		try {
			Prodotto prod=cdao.getByCod(codice);
			CategoriaDAO c=new CategoriaDAO();
			ArrayList<Categoria> elCat=new ArrayList<Categoria>();
			
			for(int i=0;i<elCat.size();i++) {
				elCat.set(i, c.getByCod(cdao.recuperaCategorie(prod).get(i)));
			}
			
			prod.setCat(elCat);
			String prodJson=jasonizzatore.toJson(prod);
		
			out.print(prodJson);
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

}
