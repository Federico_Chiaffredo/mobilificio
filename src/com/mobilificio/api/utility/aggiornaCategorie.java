package com.mobilificio.api.utility;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mobilificio.api.model.Categoria;
import com.mobilificio.api.services.CategoriaDAO;

/**
 * Servlet implementation class aggiornaCategorie
 */
@WebServlet("/aggiornaCategorie")
public class aggiornaCategorie extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String codice=request.getParameter("cod");
		String nome=request.getParameter("nom");
		String descrizione=request.getParameter("descr");
		PrintWriter out = response.getWriter();
		CategoriaDAO cdao=new CategoriaDAO();
		
		try {
			Categoria temp=cdao.getByCod(codice);
			temp.setCodice(codice);
			temp.setNome(nome);
			temp.setDescrizione(descrizione);
			cdao.update(temp);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		
		
		
	}

}
