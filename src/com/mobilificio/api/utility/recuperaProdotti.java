package com.mobilificio.api.utility;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.mobilificio.api.model.Categoria;
import com.mobilificio.api.model.Prodotto;
import com.mobilificio.api.services.CategoriaDAO;
import com.mobilificio.api.services.ProdottoDAO;

/**
 * Servlet implementation class recuperaProdotti
 */
@WebServlet("/recuperaProdotti")
public class recuperaProdotti extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		PrintWriter out = response.getWriter();
		ProdottoDAO pdao=new ProdottoDAO();
		Gson jasonizzatore=new Gson();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		
		
		try {
			ArrayList<Prodotto> elenco=pdao.getAll();
			String elencoJson=jasonizzatore.toJson(elenco);
			out.print(elencoJson);
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}

}
